import {useEffect, useState, useRef, useCallback} from 'react';
import {io} from 'socket.io-client';

export default function Home() {
    const socket = useRef()
    const [magnetometer, setMagnetometer] = useState({})
    const [engine, setEngine] = useState([])
    const [status, setStatus] = useState('Idle')
    const [gps, setGPS] = useState([])
    const onSocketConnect = useCallback(() => {
        console.log('Socket connected!')
    }, [])

    const onMagnetometerData = useCallback((data) => {
        setMagnetometer(data[0])
    }, [setMagnetometer])

    const onEngineData = useCallback((data) => {
        setEngine(data[0])
    }, [setEngine])

    const onGPSData = useCallback((data) => {
        setGPS(data[0])
    }, [setGPS])

    const startSquareSearch = useCallback(() => {
        socket.current.emit('mcuData', {mode: 'square'})
        setStatus('Field')
    }, [socket, setStatus])

    const startSingleSearch = useCallback(() => {
        socket.current.emit('mcuData', {mode: 'single'})
        setStatus('Line')
    }, [socket, setStatus])

    const stopSearch = useCallback(() => {
        socket.current.emit('mcuData', {mode: 'idle'})
        setStatus('Idle')
    }, [socket, setStatus])

    useEffect(() => {
        socket.current = io('ws://192.168.178.94:5000')
        socket.current.on('connect', onSocketConnect)
        socket.current.on('magnetometerData', onMagnetometerData)
        socket.current.on('engineData', onEngineData)
        socket.current.on('gpsData', onGPSData)
    }, [socket, onSocketConnect, onMagnetometerData, onEngineData, onGPSData])

    return (
        <div className="w-[640px] mx-auto pt-16">
            <h1 className="text-4xl mb-4">Status</h1>
            <div className="">
                <div className={`${status === 'Idle' && 'bg-yellow-600 text-yellow-900'} ${['Field', 'Line'].includes(status) && 'bg-green-400 text-green-900'} text-gray-100 py-8 text-center rounded-lg`}>{status}</div>
            </div>
            <h1 className="text-4xl mt-8 mb-4">Engine</h1>
            <div className="grid grid-cols-2 gap-x-4">
                <p className="bg-gray-200 rounded-lg text-center text-xl py-6 px-2">
                    <span className="text-gray-500 text-base block">Left</span>
                    {engine && engine[0]}
                </p>
                <p className="bg-gray-200 rounded-lg text-center text-xl py-6 px-2">
                    <span className="text-gray-500 text-base block">Right</span>
                    {engine && engine[1]}
                </p>
            </div>
            <h1 className="text-4xl mt-8 mb-4">Magnetometer</h1>
            <div className="grid grid-cols-3 gap-x-4">
                <p className="bg-gray-200 rounded-lg text-center text-xl py-6 px-2">
                    <span className="text-gray-500 text-base block">X</span>
                    {(magnetometer.x && magnetometer.x.toFixed(2)) || 'N/A'}
                </p>
                <p className="bg-gray-200 rounded-lg text-center text-xl py-6 px-2">
                    <span className="text-gray-500 text-base block">Y</span>
                    {(magnetometer.y && magnetometer.y.toFixed(2)) || 'N/A'}
                </p>
                <p className="bg-gray-200 rounded-lg text-center text-xl py-6 px-2">
                    <span className="text-gray-500 text-base block">Z</span>
                    {(magnetometer.z && magnetometer.z.toFixed(2)) || 'N/A'}
                </p>
            </div>
            <h1 className="text-4xl mt-8 mb-4">GPS</h1>
            <div className="">
                <p className="bg-gray-200 rounded-lg text-center text-xl py-6 px-2">
                    <span className="text-gray-500 text-base block">Location</span>
                    {(gps && gps.length > 0) || 'No location'}
                </p>
            </div>
            <h1 className="text-4xl mt-8 mb-4">Controls</h1>
            <div className="grid grid-cols-3 gap-x-4">
                <button className="bg-gray-800 text-gray-100 py-4 rounded-lg" onClick={stopSearch}>Idle</button>
                <button className="bg-gray-800 text-gray-100 py-4 rounded-lg" onClick={startSquareSearch}>Field</button>
                <button className="bg-gray-800 text-gray-100 py-4 rounded-lg" onClick={startSingleSearch}>Line</button>
            </div>
        </div>
    )
}